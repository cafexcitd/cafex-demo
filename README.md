[![Button](https://www.cafex.com/images/logo.png)](https://www.cafex.com/products.php) D E M O

# CaféX Demo

This is the CaféX Demo from [some Link here](#).

## To use

Add a page PHP page in root directory (same as where `index.php` is - also use this as a reference, i.e. the includes).  Then update `app/template/sb-admin-2/navigation.php` and add your link to the new page there (you'll see others so you can use that as a reference).  Multi-level pages are also possible.