<?php include_once 'app/template/head.php'; ?>

<h1 class="page-header">Callbacks</h1>

<p>This page will be used to demonstrate LA 1.37 callbacks from the consumer side.</p>

<p><button id="go">Click here</button> to start a Support session.</p>

<?php include_once 'app/template/foot.php'; ?>

<!--======= CafeX JS SDKs =======-->
<script src="https://cs-snadeem2.cafex.com:8443/assistserver/sdk/web/consumer/assist.js"></script>

<script>
    'use strict';

    // Configuration to be passed into AssistSDK.startSupport()
    var config = {
        destination: 'agent1'
    };

    // Start support session
    $('#go').click(function() {
        AssistSDK.startSupport(config);
    });

    /**
     * Callbacks
     */

    AssistSDK.onConnectionEstablished = function() {
        console.log('--------------- onConnectionEstablished');
    };

    AssistSDK.onWebcamUseAccepted = function() {
        console.log('--------------- onWebcamUseAccepted');
    };

    AssistSDK.onScreenshareRequest = function() {
        console.log('--------------- onSreenshareRequest');

        // What's the difference vs onAgentRequestedCobrowse?
    };

    AssistSDK.onInSupport = function() {
        console.log('--------------- onInSupport');
    };

    AssistSDK.onPushRequest = function() {
        console.log('--------------- onPushRequest');
    };

    AssistSDK.onDocumentReceivedSuccess = function(sharedDocument) {
        console.log('--------------- onDocumentReceivedSuccess');
    };

    AssistSDK.onDocumentReceivedError = function(sharedDocument) {
        console.log('--------------- onDocumentReceivedError');
    };

    AssistSDK.onAnnotationAdded = function(annotation, sourceName) {
        console.log('--------------- onAnnotationAdded');
    };

    AssistSDK.onAnnotationsCleared = function() {
        console.log('--------------- onAnnotationsCleared');
    };

    AssistSDK.onCobrowseActive = function() {
        console.log('--------------- onCobrowseActive');
    };

    AssistSDK.onCobrowseInactive = function() {
        console.log('--------------- onCobrowseInactive');
    };

    AssistSDK.onAgentJoinedSession = function(agent) {
        console.log('--------------- onAgentJoinedSession');
    };

    AssistSDK.onAgentRequestedCobrowse = function(agent) {
        console.log('--------------- onAgentRequestedCobrowse');

        // Prevent repeated custom alerts if user refreshes pages, goes to another page then back etc.
        if (sessionStorage.getItem('cobrowse') !== 'true') {
            bootbox.confirm('The Agent has requested a co-browsing session.  Please confirm.', function(response) {
                if (response) {
                    AssistSDK.allowCobrowseForAgent(agent);
                    sessionStorage.setItem('cobrowse', 'true');
                } else {
                    // Any issues, remove this 'else' block along with the code here
                    AssistSDK.disallowCobrowseForAgent(agent);
                    sessionStorage.setItem('cobrowse', 'false');
                }
            });
        } else {
            AssistSDK.allowCobrowseForAgent(agent);
        }
    };

    AssistSDK.onAgentJoinedCobrowse = function(agent) {
        console.log('--------------- onAgentJoinedCobrowse');
    };

    AssistSDK.onAgentLeftCobrowse = function(agent) {
        console.log('--------------- onAgentLeftCobrowse');
    };

    AssistSDK.onAgentLeftSession = function(agent) {
        console.log('--------------- onAgentLeftSession');
    };

    AssistSDK.onEndSupport = function() {
        console.log('--------------- onEndSupport');

        if (sessionStorage.getItem('cobrowse') !== null) {
            sessionStorage.removeItem('cobrowse');
        }
    };

    AssistSDK.onError = function(error) {
        console.log('--------------- onError');
    };
</script>

</body>
</html>