    </div>
    <!-- /#wrapper -->

    <!--======= 3rd Party Dependencies =======-->
    <!-- jQuery -->
    <script src="external/jquery/jquery.min.js"></script>
    <script src="external/jquery/jquery-migrate.min.js"></script>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="external/bootstrap/bootstrap.min.css">
    <script src="external/bootstrap/bootstrap.min.js"></script>
    <!-- Bootbox -->
    <script src="external/bootbox/bootbox.min.js"></script>
    <!-- metisMenu -->
    <link rel="stylesheet" href="external/metisMenu/metisMenu.css">
    <script src="external/metisMenu/metisMenu.js"></script>
    <!-- SB Admin 2 -->
    <link rel="stylesheet" href="external/css/sb-admin-2.css">
    <script src="external/js/sb-admin-2.js"></script>
    <!-- FontAwesome -->
    <link rel="stylesheet" href="external/font-awesome/css/font-awesome.min.css">