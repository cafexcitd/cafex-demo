<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags MUST come first in the head; any other head content must come AFTER these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="internal/images/icons/favicon-32x32.png" sizes="32x32">

    <title>CafêX Demo</title>
</head>
<body>
    <div id="wrapper">